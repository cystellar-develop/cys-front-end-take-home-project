import * as React from "react";
import { useState } from "react";
import MapGL from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";

const MAPBOX_TOKEN =
  "pk.eyJ1IjoiY3lzdGVsbGFybWFwYm94IiwiYSI6ImNrOGJzcXRwbTBmM3EzZXBpb3Q5bDA2cGsifQ.QHhqagUgLRUepPeTqp0jeg";

export default function Earthquake() {
  const [viewport, setViewport] = useState({
    latitude: 37.7577,
    longitude: -122.4376,
    zoom: 8,
  });

  console.log("=====");

  return (
    <MapGL
      {...viewport}
      width="100vw"
      height="100vh"
      mapStyle="mapbox://styles/mapbox/dark-v9"
      onViewportChange={setViewport}
      mapboxApiAccessToken={MAPBOX_TOKEN}
    />
  );
}
