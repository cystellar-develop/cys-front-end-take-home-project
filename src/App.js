import * as React from "react";
import "./App.css";
import "mapbox-gl/dist/mapbox-gl.css";

import Earthquake from "./Earthquake";

function App() {
  return (
    <div className="App">
      <Earthquake />
    </div>
  );
}

export default App;
