# Overview
Create an app that Visualize earthquake frequency by location using a [heatmap layer](https://docs.mapbox.com/mapbox-gl-js/style-spec/layers/#heatmap).
 The goal is to show how you can take a user story and implement it using javascript, html, and css.

 ![Earthquake Heatmap](heatmap.png)

> Time Limit: Please spend no more than a couple of hours on this. We respect your time so we do not expect you to spend a lot of time on this. Feel free to add TODO comments about what you would do if you had more time.

*Most of the boiler plate has been set up for you already. However, this project is only partially reflective of cystellar current front end technology. Our front end uses Reactjs and [react-map-gl](https://github.com/visgl/react-map-gl) for visualization, if you are not currently comfortable with Reactjs or Mapbox, feel free to rip it out and use what you are comfortable with.*


Our goal is for you to spend a few hours implementing the user story below so we can discuss your code and the choices you made with it in your next interview.

# Data source
[https://earthquake.usgs.gov/](https://earthquake.usgs.gov/) provides earthquake information. You can fetch data using their API.

## Example Query
```
GET https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2021-10-01&endtime=2021-10-31
```

You can use `starttime` and `endtime` to filter data.

> Please read the [API Documentation - Earthquake Catalog](https://earthquake.usgs.gov/fdsnws/event/1/) for more detail.

# User story
As an insurance underwriter, I want to see heatmap of earthquake frequency in a map for a given perion (start date, end date) so that I can better understand risk for a policy.

# Acceptance Criteria
Verify that I can
- Visualize earthquake frequency by location using a heatmap layer.
- Use the `https://earthquake.usgs.gov/` API to fetch earthquake data
- Add a way to filter the data by start date and end date.

# Setup
- Clone it
```
git clone git@bitbucket.org:cystellar-develop/cys-front-end-take-home-project.git
```
- Run the initial install

`npm install`

# Running

`npm start`

Open [http://localhost:3000/](http://localhost:3000/)

# Finish Up
- Push your updated code
- Create a pull request
- Respond to the email that had the link to this repository to indicate the project is complete.